﻿namespace CoreMvcDemo.Models
{
    public class FileDto
    {
        public const string FileLocation = "FileLocation";
        public string FilePath { get; set; }
    }
}
