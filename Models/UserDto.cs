﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CoreMvcDemo.Models
{
    public class UserDto
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required]
        [StringLength(10, ErrorMessage = "First name length can't be more than 10")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(5, ErrorMessage = "Last name length can't be more than 5")]
        public string LastName { get; set; }
    }
}
