﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using CoreMvcDemo.Models;
using CoreMvcDemo.Services.IService;

namespace CoreMvcDemo.Services
{
    public class User : IUser
    {
        private readonly IFileHandler _fileHelper;
        public User(IFileHandler helper)
        {
            _fileHelper = helper;
        }
        public void AddPerson(UserDto person)
        {
            if (person is null)
            {
                throw new ArgumentNullException(nameof(person));
            }

            var persons = GetPersonList();
            if (persons == null)
                persons = new List<UserDto>();

            persons.Add(person);
            _fileHelper.WriteFile(JsonConvert.SerializeObject(persons));
        }
       
        public List<UserDto> GetPersonList()
        {
            try
            {
                return _fileHelper.ReadFile<UserDto>();
            }
            catch (Exception ex)
            {
                throw;
            }
           
        }

        public string DiplayUser()
        {
            return _fileHelper.DisplayUserInfo();
        }
    }
}
