﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using CoreMvcDemo.Models;
using CoreMvcDemo.Services.IService;

namespace CoreMvcDemo.Services.Helpers
{
    public class FileHandler : IFileHandler
    {
        private string _filePath;       

        public FileHandler(IOptions<FileDto> options)
        {
            if (options is null)
            {
                throw new System.ArgumentNullException(nameof(options));
            }

            _filePath = Path.Combine(Directory.GetCurrentDirectory(), options.Value.FilePath);
        }

        public List<T> ReadFile<T>() where T : class
        {           
                string user = File.ReadAllText(_filePath);
                if (!string.IsNullOrEmpty(user))
                {
                    return JsonConvert.DeserializeObject<List<T>>(user);
                }
                return null;
            
        }

        public void WriteFile(string user)
        {           
            File.WriteAllText(_filePath, user);
           
        }

        public string DisplayUserInfo()
        {
            var fileContent = File.ReadAllText(_filePath);
            return fileContent.Length <= 0 ? "Create users to show " : fileContent;
        }
    }
}

