﻿using System.Collections.Generic;

namespace CoreMvcDemo.Services.IService
{
    public interface IFileHandler
    {
        List<T> ReadFile<T>() where T : class;
        string DisplayUserInfo();
        void WriteFile(string data);
    }
}
