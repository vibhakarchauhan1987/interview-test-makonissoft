﻿using System.Collections.Generic;
using CoreMvcDemo.Models;

namespace CoreMvcDemo.Services.IService
{
    public interface IUser
    {
        void AddPerson(UserDto person);
        string DiplayUser();
        List<UserDto> GetPersonList();
    }
}
