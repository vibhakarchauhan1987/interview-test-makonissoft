﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Diagnostics;
using CoreMvcDemo.Models;
using CoreMvcDemo.GenRepository.IGenRepo;
using System.Collections.Generic;

namespace CoreMvcDemo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;       
        private readonly IUnitOfWork _unitOfWork;

        public HomeController(ILogger<HomeController> logger,  IUnitOfWork unitOfWork)
        {
            _logger = logger;           
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            List<UserDto> users = new List<UserDto>();
            try
            {
                _logger.LogInformation("Index method called");                
                users = _unitOfWork.user.GetUserList();
            }
            catch (System.Exception ex)
            {

                throw;
            }
            return View(users);
        }


        [HttpGet]
        public IActionResult CreateUser()
        {
            return View();
        }

        

        [HttpPost]
        public IActionResult CreateUser(UserDto user)
        {
            if (user is null)
            {
                throw new System.ArgumentNullException(nameof(user));
            }

            if (!ModelState.IsValid)
            {
                return View(user);
            }

            _logger.LogInformation($"CreateUser method :{JsonConvert.SerializeObject(user)}");
            _unitOfWork.user.AddUser(user);
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        [Produces("application/json")]
        public IActionResult DisplayData()
        {
            
            return Content(_unitOfWork.user.DiplayUser());

        }
    }
}
