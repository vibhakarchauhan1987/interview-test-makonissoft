﻿using CoreMvcDemo.Models;
using CoreMvcDemo.Services.IService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CoreMvcDemo.GenRepository.GenRepo
{
    public class UserRepository : GenRepository<UserDto>, IUserRepository
    {
        private readonly IFileHandler _fileHelper;
        public UserRepository(IFileHandler helper)
        {
            _fileHelper = helper;
        }
        public void AddUser(UserDto person)
        {
            if (person is null)
            {
                throw new ArgumentNullException(nameof(person));
            }

            var persons = GetUserList();
            if (persons == null)
                persons = new List<UserDto>();

            persons.Add(person);
            _fileHelper.WriteFile(JsonConvert.SerializeObject(persons));
        }

        public List<UserDto> GetUserList()
        {
            return _fileHelper.ReadFile<UserDto>();

        }

        public string DiplayUser()
        {
            return _fileHelper.DisplayUserInfo();
        }
    }
}
