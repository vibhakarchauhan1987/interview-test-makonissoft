﻿using CoreMvcDemo.GenRepository.IGenRepo;
using Microsoft.Extensions.DependencyInjection;

namespace CoreMvcDemo.GenRepository.GenRepo
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
           
            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IUnitOfWork, UnitOfWork>();           
            return services;
        }
    }
}
