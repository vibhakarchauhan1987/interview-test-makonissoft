﻿using CoreMvcDemo.GenRepository.IGenRepo;
using System;

namespace CoreMvcDemo.GenRepository.GenRepo
{
    public class UnitOfWork : IUnitOfWork
    {
        
        public IUserRepository user { get; }

        public UnitOfWork(IUserRepository userRepository)
        {
            
            this.user = userRepository;

        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.Dispose(true);
            }
        }

       
    }
}
