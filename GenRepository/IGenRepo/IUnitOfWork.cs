﻿using System;

namespace CoreMvcDemo.GenRepository.IGenRepo
{
    public interface IUnitOfWork:IDisposable
    {
        
        IUserRepository user { get;}

    }
}
