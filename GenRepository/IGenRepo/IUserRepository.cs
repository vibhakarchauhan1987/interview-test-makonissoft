﻿using CoreMvcDemo.Models;
using System.Collections.Generic;

namespace CoreMvcDemo.GenRepository
{
    public interface IUserRepository : IGenRepository<UserDto>
    {
        void AddUser(UserDto user);
        string DiplayUser();
        List<UserDto> GetUserList();
    }
}
