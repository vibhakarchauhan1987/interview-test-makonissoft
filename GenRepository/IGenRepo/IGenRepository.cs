﻿using System.Collections.Generic;

namespace CoreMvcDemo.GenRepository
{
    public interface IGenRepository<T> where T : class
    {

        string Get();
        void Add(T entity);
        List<T> GetAll();
      
    }
}
